/*
#University of Hawaii, College of Engineering
#brief   Lab07 - My First Cat - EE 205 - Spr 2022
#
# @file    hello3.cpp
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @1 Mar 2022
*/

#include <iostream>
using namespace std;

class Cat {
   public:
      void sayHello() {
         cout<<"Meow"<<endl;
 }
};

int main() {
   Cat myCat;
   myCat.sayHello();
   return 0;
}

