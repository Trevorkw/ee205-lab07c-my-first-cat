/*
#University of Hawaii, College of Engineering
#brief   Lab07 - My First Cat - EE 205 - Spr 2022
#
#
# @file    hello2.cpp
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @1 Mar 2022
*/

#include <iostream>


int main() {
   std::cout <<"Hello World!"<<std::endl;
   return 0;
}

